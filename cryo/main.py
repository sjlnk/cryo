import asyncio
import brotli
import os
import json
import shutil
import itertools
import logging
import sys
import random
import argparse
import tempfile
import subprocess
import httpx
import hashlib
import tomlkit
import re
import xdg
import pandas as pd
import numpy as np
from binascii import hexlify, unhexlify
from base64 import b64encode, b64decode
from copy import copy, deepcopy
from time import time, monotonic
from datetime import datetime
from pprint import pprint, pformat
from loguru import logger as Lr
from pathlib import Path
from prompt_toolkit import PromptSession
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.validation import (
    Validator,
    ValidationError,
)
from cryo.lib.asyncio import (
    create_managed_task,
    wait_first,
)
from cryo.lib.logging import (
    setup_logging,
    Format as LoggingFormat,
)
from cryo.lib.async_app import (
    run_app,
    create_managed_task,
)
from collections import (
    defaultdict,
    deque,
)
from aioaws.s3 import (
    S3Client,
    S3Config,
    S3Object,
    S3ObjectVersion,
    S3Transfer,
)
from aioaws.exceptions import RequestError
from pandas import DataFrame, Series, Timestamp, Timedelta


###############################################################################


class GlobalState:
    pass

g = GlobalState()
g.shutdown_event = asyncio.Event()


###############################################################################


class DictFile(dict):


    def __init__(self, path, serializer=None, binary=True):
        path = Path(path)
        if not serializer:
            serializer = json
            binary = False
        self._path = path
        self._binary = binary
        self._serializer = serializer
        data = {}
        if path.exists():
            with path.open("rb" if self._binary else "r") as f:
                data = serializer.load(f)
        super().__init__(data)

    
    def __setitem__(self, k, v):
        super().__setitem__(k, v)
        with self._path.open("wb" if self._binary else "w") as f:
            self._serializer.dump(self, f)


    def update(self, E):
        if hasattr(E, "keys"):
            for k in E:
                self[k] = E[k]
        else:
            for k, v in E:
                self[k] = v


###############################################################################


class Compressor:


    def __init__(self, project_data):
        self._project_data = project_data


    async def compress(self):
        project_apdn = Path(self._project_data["project_root"])
        archive_apfn = project_apdn / self._project_data["archive_rpfn"]
        compressed_rpfn = self._output_rpfn()
        compressed_apfn = project_apdn / compressed_rpfn
        if compressed_apfn.exists():
            f_hash = await sha256_b64(compressed_apfn)
            if (
                f_hash == self._project_data.get("compressed_hash") and 
                self._project_data["compression_args"] == g.args.compression_args
            ):
                return
        await self._do_compress(archive_apfn, compressed_apfn)
        self._project_data["compression_args"] = g.args.compression_args
        self._project_data["compressed_hash"] = await sha256_b64(compressed_apfn)
        self._project_data["compressed_rpfn"] = compressed_rpfn


    async def decompress(self):
        project_apdn = Path(self._project_data["project_root"])
        compressed_apfn = project_apdn / self._project_data["compressed_rpfn"]
        archive_apfn = project_apdn / self._project_data["metadata"]["archive_name"]
        if archive_apfn.exists():
            f_hash = await sha256_b64(compressed_apfn)
            if f_hash == self._project_data["metadata"]["archive_hash"]:
                return
        await self._do_decompress(compressed_apfn)


    async def get_version(self):
        raise NotImplementedError()
    

    async def get_name(self):
        raise NotImplementedError()

    
    async def get_format(self):
        raise NotImplementedError()


class XZCompressor(Compressor):


    def _output_rpfn(self):
        return self._project_data["archive_rpfn"] + ".xz"


    async def _do_compress(self, archive_apfn, compressed_apfn):
        compression = g.args.compression_args or "-0"
        Lr.info(f'compressing archive with xz (args: "{compression}") ...')
        await shell(f'xz --compress --threads=0 --keep --check=sha256 '
                    f'--force --verbose {compression} {archive_apfn}')


    async def _do_decompress(self, compressed_apfn):
        Lr.info("decompressing compressed archive ...")
        await shell(f'xz --decompress --keep --check=sha256 --force --verbose '
                    f'{compressed_apfn}')


    async def get_version(self):
        return await shell("xz --version", True)
    

    async def get_name(self):
        return "xz"
    

    async def get_format(self):
        return "xz"


###############################################################################


async def encrypt(cypher, key, data, **kwargs):


    if cypher == "age":
        key_data = g.encryption_keys[key]
        input_apfn = Path(tempfile.mktemp())
        output_apfn = Path(tempfile.mktemp())
        try:
            with input_apfn.open("wb") as f:
                f.write(data)
            await shell(f'age --encrypt --recipient={key_data["public"]} '
                        f'{input_apfn} > {output_apfn}')
            with output_apfn.open("rb") as f:
                return f.read()
        finally:
            input_apfn.unlink(missing_ok=True)
            output_apfn.unlink(missing_ok=True)

    elif cypher == "gpg_symmetric":
        input_apfn = Path(tempfile.mktemp())
        output_apfn = Path(tempfile.mktemp())
        try:
            with input_apfn.open("wb") as f:
                f.write(data)
            await shell(f'echo {key} | gpg -q --symmetric '
                        f'--cipher-algo AES256 --batch --passphrase-fd 0 '
                        f'--output {output_apfn} {input_apfn}')
            with output_apfn.open("rb") as f:
                return f.read()
        finally:
            input_apfn.unlink(missing_ok=True)
            output_apfn.unlink(missing_ok=True)

    else:
        raise ValueError(f'{cypher=} is not implemented')
            

async def decrypt(cypher, key, cyphertext):

    if cypher == "age":
        key_data = g.encryption_keys[key]
        input_apfn = Path(tempfile.mktemp())
        output_apfn = Path(tempfile.mktemp())
        try:
            with input_apfn.open("wb") as f:
                f.write(cyphertext)
            await shell(f'age --decrypt --identity={key_data["path"]} '
                        f'{input_apfn} > {output_apfn}')
            with output_apfn.open("rb") as f:
                return f.read()
        finally:
            input_apfn.unlink(missing_ok=True)
            output_apfn.unlink(missing_ok=True)

    elif cypher == "gpg_symmetric":
        input_apfn = Path(tempfile.mktemp())
        output_apfn = Path(tempfile.mktemp())
        try:
            with input_apfn.open("wb") as f:
                f.write(cyphertext)
            await shell(f'echo {key} | gpg -q --decrypt --batch '
                        f'--passphrase-fd 0 '
                        f'{input_apfn} > {output_apfn}')
            with output_apfn.open("rb") as f:
                return f.read()
        finally:
            input_apfn.unlink(missing_ok=True)
            output_apfn.unlink(missing_ok=True)

    else:
        raise ValueError(f'{cypher=} is not implemented')


class Encrypter:
    

    def __init__(self, project_data):
        self._project_data = project_data


    async def encrypt(self):
        project_apdn = Path(self._project_data["project_root"])
        compressed_apfn = project_apdn / self._project_data["compressed_rpfn"]
        encrypted_rpfn = self._add_suffix(self._project_data["compressed_rpfn"])
        encrypted_apfn = project_apdn / encrypted_rpfn
        if encrypted_apfn.exists():
            f_hash = await sha256_b64(encrypted_apfn)
            if f_hash == self._project_data.get("encrypted_hash"):
                return
        await self._do_encrypt(compressed_apfn, encrypted_apfn)
        self._project_data["encrypted_hash"] = await sha256_b64(encrypted_apfn)
        self._project_data["encrypted_rpfn"] = encrypted_rpfn


    async def decrypt(self):
        project_apdn = Path(self._project_data["project_root"])
        compressed_rpfn = self._remove_suffix(
                self._project_data["metadata"]["object_name"])
        compressed_apfn = project_apdn / compressed_rpfn
        encrypted_apfn = project_apdn / self._project_data["metadata"]["object_name"]
        if compressed_apfn.exists():
            f_hash = await sha256_b64(compressed_apfn)
            if f_hash == self._project_data.get("compressed_hash"):
                return
        await self._do_decrypt(encrypted_apfn, compressed_apfn)
        self._project_data["compressed_hash"] = await sha256_b64(compressed_apfn)
        self._project_data["compressed_rpfn"] = compressed_rpfn


    async def get_version(self):
        raise NotImplementedError()
    

    async def get_name(self):
        raise NotImplementedError()


class SymmetricGPGEncrypter(Encrypter):


    def _add_suffix(self, x):
        return x + ".gpg"


    def _remove_suffix(self, x):
        if not x.endswith(".gpg"):
            raise ValueError(f'unexpected filename: {x}')
        return x[:-4]


    async def _do_encrypt(self, compressed_apfn, encrypted_apfn):
        Lr.info("encrypting compressed archive ...")
        vault = g.config["vaults"][g.config["active_vault"]]
        passphrase = vault["passphrase"]
        await shell(f'echo {passphrase} | gpg -q --symmetric '
                    f'--cipher-algo AES256 --batch --passphrase-fd 0 '
                    f'{compressed_apfn}')


    async def _do_decrypt(self, encrypted_apfn, compressed_apfn):
        Lr.info("decrypting encrypted compressed archive ...")
        key_name = self._project_data["metadata"]["encryption_key_name"]
        passphrase = vault["passphrase"]
        await shell(f'echo {passphrase} | gpg -q --decrypt --batch '
                    f'--passphrase-fd 0 '
                    f'{encrypted_apfn} > {compressed_apfn}')


    async def get_version(self):
        return await shell("gpg --version | head -2", True)
    

    async def get_name(self):
        return "gpg"



class AgeEncrypter(Encrypter):
    

    def _add_suffix(self, x):
        return x + ".age"


    def _remove_suffix(self, x):
        if not x.endswith(".age"):
            raise ValueError(f'unexpected filename: {x}')
        return x[:-4]


    async def _do_encrypt(self, compressed_apfn, encrypted_apfn):
        Lr.info("encrypting compressed archive ...")
        vault = g.config["vaults"][g.config["active_vault"]]
        key_data = g.encryption_keys[vault["encryption_key"]]
        await shell(f'age --encrypt --recipient={key_data["public"]} '
                    f'{compressed_apfn} > {encrypted_apfn}')


    async def _do_decrypt(self, encrypted_apfn, compressed_apfn):
        Lr.info("decrypting encrypted compressed archive ...")
        key_name = self._project_data["metadata"]["encryption_key_name"]
        key_data = g.encryption_keys[key_name]
        await shell(f'age --decrypt --identity={key_data["path"]} '
                    f'{encrypted_apfn} > {compressed_apfn}')


    async def get_version(self):
        return await shell("age --version", True)
    

    async def get_name(self):
        return "age"
    

###############################################################################


class CryoIndex(dict):
    

    def __init__(self, location):
        self._location = location


    async def initialize(self):
        vault_cache_apdn = xdg.xdg_cache_home() / "cryo" / self._location
        index_apfn = vault_cache_apdn / "index"
        if index_apfn.exists():
            with index_apfn.open("rb") as f:
                raw_data = f.read()
            self._init_hash = b64encode(hashlib.sha256(raw_data).digest()).decode()
            envelope, _, cyphertext = raw_data.partition(b"\n")
            envelope = json.loads(envelope)
            if "key" in envelope:
                encryption_key = envelope["key"]
            else:
                vault_name = g.config["active_vault"]
                vault_cfg = g.config["vaults"][vault_name]
                encryption_key = vault_cfg["passphrase"]
            decrypted = await decrypt(
                cypher=envelope["cypher"],
                key=encryption_key,
                cyphertext=cyphertext
            )
            data_dict = json.loads(brotli.decompress(decrypted))
        else:
            data_dict = {}
            self._init_hash = None
        self._modified_keys = set()
        self._files = [ index_apfn ]
        super().__init__(data_dict)


    def __setitem__(self, k, v):
        if k in self and v == self[k]:
            return
        self._modified_keys.add(k)
        super().__setitem__(k, v)


    def __delitem__(self, k):
        if k not in self:
            raise KeyError(k)
        self._modified_keys.add(k)
        super().__delitem__(k)


    def modified_keys(self):
        return deepcopy(self._modified_keys)


    def get_files(self):
        return deepcopy(self._files)


    async def dump_to_cache(self, location, cypher, encryption_key, save_key): 
        vault_cache_apdn = xdg.xdg_cache_home() / "cryo" / location
        vault_cache_apdn.mkdir(parents=True, exist_ok=True)
        index_apfn = vault_cache_apdn / "index"
        envelope = {"cypher": cypher}
        if save_key:
            envelope["key"] = encryption_key
        # brotli seems to compress metadata better than lzma or gzip ...
        index_json = json.dumps(self).encode()
        Lr.info(f'uncompressed index data size: {len(index_json):,}')
        compressed = brotli.compress(index_json)
        Lr.info(f'compressed index data size: {len(compressed):,}')
        cyphertext = await encrypt(
                **envelope, key=encryption_key, data=compressed)
        Lr.info(f'encrypted index data size: {len(cyphertext):,}')
        envelope["compressor_lib"] = "pypi:brotli"
        envelope["compressor_lib_version"] = brotli.__version__

        assert cypher == "gpg_symmetric"
        encrypter = SymmetricGPGEncrypter(None)
        envelope["encrypter_sw_version"] = await encrypter.get_version()
        envelope["encrypter_sw"] = await encrypter.get_name()

        data = json.dumps(envelope).encode() + b"\n" + cyphertext
        Lr.info(f'size of metadata: {sizeof_fmt_si(len(data))}')
        with index_apfn.open("wb") as f:
            f.write(data)


###############################################################################


class Vault:


    def __init__(self, project_data):
        self._project_data = project_data


    async def close_connection(self):
        pass

    
    async def upload(self):
        raise NotImplementedError()


    async def download(self):
        raise NotImplementedError()


    async def defreeze(self):
        raise NotImplementedError()


    async def refresh_index(self):
        raise NotImplementedError()


    async def save_index(self):
        raise NotImplementedError()


    async def truncate_index(self):
        raise NotImplementedError()


    async def truncate_index_versions(self):
        raise NotImplementedError()


    async def truncate_objects(self):
        raise NotImplementedError()


    async def truncate_object_versions(self):
        raise NotImplementedError()


    async def print_stats(self):
        raise NotImplementedError()


    def standardize_storage_class(self, x):
        raise NotImplementedError()


class S3Vault(Vault):

    
    def __init__(self, project_data, timeout=None):
        super().__init__(project_data)
        self._vault_name = g.config["active_vault"]
        self._vault_cfg = g.config["vaults"][self._vault_name]
        if not timeout:
            timeout = httpx.Timeout(30)
        self._http_client = httpx.AsyncClient(timeout=timeout)
        s3_config = S3Config(
            self._vault_cfg["aws_key"],
            self._vault_cfg["aws_secret"],
            self._vault_cfg["aws_region"],
            self._vault_cfg["s3_bucket"]
        )
        self._client = S3Client(self._http_client, s3_config, retry=True)


    async def close_connection(self):
        await self._http_client.aclose()


    async def upload(self):

        # TODO: parse part size cmd line argument

        tx = S3Transfer(self._client)
        pdata = self._project_data
        project_apdn = Path(pdata["project_root"])
        encrypted_apfn = project_apdn / pdata["encrypted_rpfn"]
        storage_class = g.args.storage_class or \
                self._vault_cfg["default_storage_class"]
        storage_class = self.standardize_storage_class(storage_class)
        object_size = encrypted_apfn.stat().st_size
        upload_id = None

        if (part_size := pdata.get("upload_part_size", -1)) >= g.args.min_part_size:
            upload_id = pdata.get("aws_upload_id")
            Lr.info(f'resuming previous interrupted multipart upload, '
                    f'upload_id: {upload_id} ...')
        else:
            # S3 supports uploads with maximum of 10000 parts.
            # Leaving a little safety buffer.
            part_size = max(int(np.ceil(object_size / 9990)),
                            g.args.min_part_size)
            Lr.debug(f'object size: {object_size:,} bytes')
            Lr.debug(f'part size: {part_size:,} bytes')
            assert part_size * 10000 >= object_size
            pdata["upload_part_size"] = part_size

        Lr.info(f'part size: {sizeof_fmt_si(part_size)}')

        upload_args = {
            "key": f'{self._vault_cfg["cryo_root"]}/{pdata["project_hash"]}',
            "data_spec": encrypted_apfn,
            "storage_class": storage_class,
            "upload_id": upload_id, 
            "concurrency": g.args.concurrency,
            "part_size": part_size,
        }
        # TODO: add aws_upload_id invalidation logic (when input data changed)
        while True:
            try:
                async for yd in tx.upload(**upload_args):
                    if yd["msg_type"] == "multipart":
                        pdata["aws_upload_id"] = yd["data"]
                    Lr.info(f'{yd["msg_type"]} {yd["total_bytes_uploaded"]}')
            except (httpx.ConnectError, httpx.ReadTimeout, httpx.WriteTimeout,
                    httpx.ReadError, httpx.WriteError) as err:
                sleep_secs = 10
                Lr.info(f'{err.__class__.__name__} when uploading, '
                        f'retrying in {sleep_secs} seconds ...')
                await asyncio.sleep(sleep_secs)
            else:
                break


    async def download(self):

        pdata = self._project_data
        key = f'{self._vault_cfg["cryo_root"]}/{pdata["project_hash"]}'
        project_apdn = Path(pdata["project_root"])
        encrypted_apfn = project_apdn / pdata["metadata"]["object_name"]
        size = pdata["metadata"]["object_size"]
        
        if encrypted_apfn.exists():
            total_bytes = encrypted_apfn.stat().st_size
            if total_bytes == size:
                byte_range = None
            elif total_bytes > size:
                raise RuntimeError(
                        f'downloaded ({total_bytes}) more than size ({size})')
            else:
                byte_range = (total_bytes, size)
                Lr.info(f'{total_bytes} already downloaded')
        else:
            total_bytes = 0
            byte_range = None

        async with self._client.stream_object(key, byte_range=byte_range) as r:
            assert size == int(r.headers["content-length"])
            csum_s3 = r.headers["x-amz-checksum-sha256"]
            n_bytes = 0
            if total_bytes != size:
                with encrypted_apfn.open("ab") as f:
                    async for chunk in r.aiter_raw(g.args.chunk_size):
                        n_bytes += len(chunk)
                        total_bytes += len(chunk)
                        f.write(chunk)
                        Lr.info(f'{n_bytes} downloaded')
            else:
                Lr.info("object downloaded previously")
        csum = await sha256_b64(encrypted_apfn)
        assert csum == csum_s3
    

    async def defreeze(self):
        pdata = self._project_data
        key = f'{self._vault_cfg["cryo_root"]}/{pdata["project_hash"]}'
        try:
            res = await self._client.restore_object(
                key,
                g.args.days,
                restoration_speed=g.args.speed,
            )
            if not res:
                Lr.info("object is already defreezed")
                return
            # Lr.info(pformat(res))
            Lr.info("object defreezing initiated")
        except RequestError as err:
            if err.status == 409:
                Lr.info("object defreezing is already in progress")
            else:
                raise
        if not g.args.wait:
            return
        Lr.info("waiting for defreezing to finish ...") 
        res = await self._client.get_object(key, header_only=True)
        if 'ongoing-request="true"' in res.get("x-amz-restore", ""):
            Lr.info(f'days to keep: {res["x-amz-restore-expiry-days"]}')
            Lr.info(f'storage class: {res["x-amz-storage-class"]}')
            Lr.info(f'speed: {res["x-amz-restore-tier"]}')
            Lr.info(f'request date: {res["x-amz-restore-request-date"]}')
        else:
            Lr.info("done") 
            return
        await asyncio.sleep(10)
        while True:
            res = await self._client.get_object(key, header_only=True)
            if 'ongoing-request="true"' in res.get("x-amz-restore", ""):
                ts_start = Timestamp(res["x-amz-restore-request-date"])
                ts_now = Timestamp.utcnow()
                td = ts_now - ts_start
                Lr.info(f'time elapsed: {td}')
            else:
                break
            await asyncio.sleep(60)
        Lr.info("done")


    async def refresh_index(self):
        cryo_root = self._vault_cfg["cryo_root"]
        location = f'{self._vault_name}/{cryo_root}'
        self.index = CryoIndex(location)
        await self.index.initialize()
        index_updated = False
        for apfn in self.index.get_files():
            if not apfn.exists():
                apfn.parent.mkdir(parents=True, exist_ok=True)
                csum = None
            else:
                csum = await sha256_b64(apfn)
            key = f'{cryo_root}/{apfn.parts[-1]}'
            while True:
                try:
                    async with self._client.stream_object(key) as r:
                        if r.status_code == 404:
                            Lr.info(f'index file "{key}" does not exist ...')
                        elif r.status_code == 403:
                            raise RequestError(r)
                        else:
                            csum_s3 = r.headers["x-amz-checksum-sha256"]
                            if csum_s3 != csum:
                                Lr.info(f'fetching index file "{key}" ...')
                                index_updated = True
                                with apfn.open("wb") as f:
                                    async for chunk in r.aiter_raw(g.args.chunk_size):
                                        f.write(chunk)
                            csum = await sha256_b64(apfn)
                            assert csum == csum_s3
                except (httpx.ConnectError, httpx.ReadTimeout, httpx.WriteTimeout,
                        httpx.ReadError, httpx.WriteError) as err:
                    sleep_secs = 10
                    Lr.info(f'{err.__class__.__name__} when downloading index, '
                            f'retrying in {sleep_secs} seconds ...')
                    await asyncio.sleep(sleep_secs)
                else:
                    break
        if index_updated:
            self.index = CryoIndex(location)
            await self.index.initialize()


    async def save_index(self):

        if not self.index.modified_keys():
            Lr.info("index is not modified, no changes to save")
            return
        location = f'{self._vault_name}/{self._vault_cfg["cryo_root"]}'

        await self.index.dump_to_cache(
            location=location,
            cypher="gpg_symmetric",
            encryption_key=self._vault_cfg["passphrase"],
            save_key=False
        )
        for apfn in self.index.get_files():
            with apfn.open("rb") as f:
                data = f.read()
            cryo_root = self._vault_cfg["cryo_root"]
            await self._client.put_object(
                f'{cryo_root}/{apfn.parts[-1]}',
                data
            )


    async def truncate_index(self):
        await self.refresh_index()
        cryo_root = self._vault_cfg["cryo_root"]
        objects, _ = await self._client.list_objects(prefix=f'{cryo_root}/')
        objects = { x.key.split("/")[-1]: x for x in objects }
        keys_to_del = [x for x in self.index if x not in objects]
        if not keys_to_del:
            return
        for k in keys_to_del:
            Lr.info(pformat(self.index[k]))
            del self.index[k]

        Lr.info(f'deleting {len(keys_to_del)} keys from the index: ')
        Lr.info(f'number of keys in the index after truncation: {len(self.index)}')
        res = await prompt_bool("continue? ")
        if not res:
            return
        await self.save_index()


    async def truncate_index_versions(self):
        await self.refresh_index()
        for apfn in self.index.get_files():
            key = f'{self._vault_cfg["cryo_root"]}/{apfn.parts[-1]}'
            res = await self._client.truncate_versions(
                key=key,
                keep=5,  # keep 5 versions for safety
                verbose=True,
            )
            if res:
                Lr.info(f'{len(res)} old versions of {key} deleted')


    async def truncate_objects(self):
        await self.refresh_index()
        cryo_root = self._vault_cfg["cryo_root"]
        index_keys = {f'{cryo_root}/{x.parts[-1]}' for x in self.index.get_files()}

        objects, _ = await self._client.list_objects(prefix=f'{cryo_root}/')
        objects = [x for x in objects if x.key not in index_keys]
        Lr.info(f'{len(objects)} unique objects in the vault')
        for obj in objects:
            obj_hash = obj.key.split("/")[-1]
            assert obj.key not in index_keys
            if obj_hash in self.index:
                continue
            Lr.info(f'{obj_hash} does not exist in the index')
            Lr.info(pformat(obj))
            res = await prompt_bool("delete? ")
            if not res:
                continue
            res = await self._client.truncate_versions(
                key=obj.key,
                keep=0,
                verbose=True
            )
            Lr.info(f'deleted object ({len(res)} versions)')


    async def print_stats(self, all_obj_versions=None):
        await self.refresh_index()
        cryo_root = self._vault_cfg["cryo_root"]
        index_keys = {f'{cryo_root}/{x.parts[-1]}' for x in self.index.get_files()}
        if not all_obj_versions:
            all_obj_versions, _ = await self._client.list_object_versions(
                    prefix=f'{cryo_root}/')
        total_sizes = defaultdict(lambda: 0)
        for x in all_obj_versions:
            total_sizes[x.storage_class] += x.size
        total_sizes = {k: sizeof_fmt_si(v) for k, v in total_sizes.items()} 
        total_size = sum([x.size for x in all_obj_versions])
        Lr.info(pformat(total_sizes))
        Lr.info(f'aggregate size of the vault: {sizeof_fmt_si(total_size)}')
        Lr.info(f'object versions count: {len(all_obj_versions)}')


    async def truncate_object_versions(self):

        await self.refresh_index()
        cryo_root = self._vault_cfg["cryo_root"]
        index_keys = {f'{cryo_root}/{x.parts[-1]}' for x in self.index.get_files()}
        all_obj_versions, _ = await self._client.list_object_versions(
                prefix=f'{cryo_root}/')
        await self.print_stats(all_obj_versions)

        all_keys = {x.key for x in all_obj_versions}
        for key in all_keys:
            if key in index_keys:
                continue
            versions = [x for x in all_obj_versions if x.key == key]
            if len(versions) == 1:
                continue
            metadata = self.index.get(key.split("/")[-1])
            if metadata:
                Lr.info(pformat(metadata))
            else:
                Lr.info("no metadata in index")
            old_versions = [x for x in versions if not x.is_latest]
            Lr.info(pformat(old_versions))
            Lr.info(f'deleting {len(old_versions)} old versions of {key} ...')
            res = await prompt_bool("continue? ")
            if not res:
                continue
            res = await self._client.delete_objects(old_versions, verbose=True)
            Lr.info(f'deleted {len(res)} old versions of {key}')


    async def delete_unfinished_uploads(self):
        multipart_uploads = (await self._client.list_multipart_uploads())[0]
        if multipart_uploads:
            Lr.info(f'deleting {len(multipart_uploads)} '
                    f'unfinished multipart uploads ...')
            for x in multipart_uploads:
                await self._client.abort_multipart_upload(x.key, x.upload_id)
            Lr.info(f'deleted {len(multipart_uploads)} unfinished '
                    f'multipart uploads')


    def standardize_storage_class(self, x):
        return x.upper()


###############################################################################


def check_if_search_matches(long_hash, metadata):
    if len(g.args.generic_kw) >= 5 and long_hash.startswith(g.args.generic_kw):
        return True
    rexp = re.compile(g.args.generic_kw)
    for key in ("name", "comment", "source_path"):
        if key in metadata:
            if rexp.fullmatch(metadata[key]):
                return True
    for tag in metadata.get("tags", []):
        if rexp.fullmatch(tag):
            return True
    return False


async def search_and_select_object():
    raw_metadata = await search_for_matching_objects()
    if not raw_metadata:
        Lr.info("no matching search results")
        return None, None 
    if len(raw_metadata) > 1:
        df = print_search_results(raw_metadata, True)    
        selection = await prompt_integer("select object: ", 1, len(df))
        project_hash = df.loc[selection, "full_hash"]
    else:
        project_hash = next(iter(raw_metadata))
    metadata = raw_metadata[project_hash]
    return project_hash, metadata


async def search_for_matching_objects():
    vault = S3Vault(None)
    await vault.refresh_index()
    res = {}
    for long_hash, metadata in vault.index.items():
        metadata = {k: v for k, v in metadata.items() if v is not None}
        if not check_if_search_matches(long_hash, metadata):
            continue
        res[long_hash] = metadata
    await vault.close_connection()
    return res


def print_search_results(raw_metadata, numeric_ids):
    res = []
    for long_hash, metadata in raw_metadata.items():
        d = {}
        def assoc(target_key, source_key=None):
            source_key = source_key or target_key
            if source_key in metadata:
                d[target_key] = metadata[source_key]
            else:
                d[target_key] = None

        d["hash"] = long_hash
        assoc("size", "archive_size")
        assoc("content_type")
        assoc("name")
        assoc("comment")
        assoc("tags")
        assoc("storage_class")
        assoc("path", "source_path")
        assoc("mtime", "source_mtime")
        res.append(d)
    df = DataFrame(res)
    df["full_hash"] = df["hash"]
    df["hash"] = df["hash"].apply(lambda x: x[:10])
    if not numeric_ids:
        df = df.set_index("hash")
    else:
        df.index = range(1, len(df) + 1)
    df["mtime"] = df["mtime"].apply(lambda x: Timestamp.fromtimestamp(x).date())
    df["path"] = df["path"].apply(lambda x: Path(x).parts[-1])
    df["size"] = df["size"].apply(sizeof_fmt_si)
    df["name/path"] = df["name"]
    df.loc[pd.isnull(df["name"]), "name/path"] = df["path"]
    cols = [
        "name/path",
        "size",
        "mtime",
        "tags",
        "storage_class",
        "comment",
    ]
    if numeric_ids:
        cols.insert(0, "hash")
    cols = [x for x in cols if x in df.columns]
    if g.args.sort_by:
        df = df.sort_values(g.args.sort_by)
    df_to_print = df[cols]
    pd.options.display.max_colwidth = 40
    pd.options.display.width = 400
    # os.environ["COLUMNS"] = "400"
    Lr.info(df_to_print)
    return df


###############################################################################


def sizeof_fmt(num, suffix="B"):
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


def sizeof_fmt_si(num, suffix="B"):
    for unit in ["", "K", "M", "G", "T", "P", "E", "Z"]:
        if abs(num) < 1000.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1000.0
    return f"{num:.1f}Yi{suffix}"


async def shell(cmd, pipe=False):
    if pipe:
        p = await asyncio.create_subprocess_shell(cmd, stdout=subprocess.PIPE)
        res = await p.communicate()
        res = res[0].decode().strip()
    else:
        p = await asyncio.create_subprocess_shell(cmd)
        res = None
    await p.wait()
    if p.returncode != 0:
        raise RuntimeError(
                f'shell command "{cmd}" returned exit code {p.returncode}')
    return res


def get_project_apdn(project_hash, suffix):
    tmp_apdn = os.environ.get("TMP", os.environ.get("TEMP", "/tmp"))
    tmp_apdn = Path(tmp_apdn)
    if suffix:
        return tmp_apdn / f'{project_hash}-{suffix}'
    return tmp_apdn / f'{project_hash}'


async def sha256_b64(x):
    if isinstance(x, Path):
        return await shell(
                f'nix hash file --base64 --type sha256 {x}', True)
        # hash_hex = await shell(f'sha256sum --binary {x}', True)
        # hash_hex = hash_hex.split()[0].strip()
        # return b64encode(unhexlify(hash_hex)).decode()
    return b64encode(hashlib.sha256(x).digest()).decode() 


###############################################################################


class IntegerValidator(Validator):


    def __init__(self, min_value=None, max_value=None):
        self._min_value = min_value
        self._max_value = max_value


    def validate(self, document):
        text = document.text
        if not text:
            raise ValidationError()
        if text.isdigit():
            value = int(text)
            if (
                (self._min_value is not None and value < self._min_value) or
                (self._max_value is not None and value > self._max_value)
            ):
                raise ValidationError(message="input out of range")
        else:
            i = 0
            # Get index of first non numeric character.
            # We want to move the cursor here.
            for i, c in enumerate(text):
                if not c.isdigit():
                    break
            raise ValidationError(
                message="input contains non-numeric characters",
                cursor_position=i
            )


class SelectionValidator(Validator):

    
    def __init__(self, selections, ignore_case=False, aliases=None):
        self._ignore_case = ignore_case
        if ignore_case:
            selections = [x.lower() for x in selections]
            if aliases:
                aliases = {k.lower(): v.lower() for k, v in aliases.items()}
        selections = set(selections)
        if aliases:
            assert not set(aliases.values()) - selections
            for v in aliases.values():
                assert v in selections
        self._selections = selections
        self._aliases = aliases
        self._selections_and_aliases = self._selections | set(self._aliases)


    def validate(self, document):
        text = document.text
        if not text:
            raise ValidationError()
        if self._ignore_case:
            text = text.lower()
        if text not in self._selections_and_aliases:
            raise ValidationError(
                message='invalid input',
            )


    def get_selections_str(self, separator="/"):
        return "[" + separator.join(self._selections) + "]"


    def resolve_selection(self, x):
        if x in self._selections:
            return x
        return self._aliases[x]



class BooleanValidator(SelectionValidator):


    def __init__(self):
        aliases = {
            "yes": "y",
            "no": "n",
        }
        super().__init__({"y", "n"}, ignore_case=True, aliases=aliases)


    def resolve_selection(self, x):
        x = super().resolve_selection(x)
        return True if x == "y" else False


async def prompt_integer(msg, min_value=None, max_value=None):
    session = PromptSession()
    with patch_stdout():
        validator = IntegerValidator(min_value=min_value, max_value=max_value)
        res = await session.prompt_async(msg, validator=validator)
        return int(res)


async def prompt_bool(msg):
    if g.args.confirm_all:
        return True
    session = PromptSession()
    with patch_stdout():
        validator = BooleanValidator()
        msg += validator.get_selections_str() + " "
        res = await session.prompt_async(msg, validator=validator)
        return validator.resolve_selection(res)


async def prompt_str(msg):
    session = PromptSession()
    with patch_stdout():
        return await session.prompt_async(msg)



###############################################################################


async def action_freeze():

    if g.args.project:

        project_hash = g.args.project
        project_apdn = get_project_apdn(project_hash, "upload")

        Lr.info(f'working directory: {project_apdn}')

        project_data_apfn = project_apdn / "project.json"
        project_data = DictFile(project_data_apfn)

        project_data["archive_rpfn"] = "archive.nar"
        nar_apfn = project_apdn / project_data["archive_rpfn"]

    else:

        apxn = Path(g.args.path).absolute()
        _, tmp_apfn = tempfile.mkstemp()
        tmp_apfn = Path(tmp_apfn)

        Lr.info(f'creating a nar archive {tmp_apfn} ...')

        await shell(f'nix nar dump-path \'{g.args.path}\' > {tmp_apfn}')

        project_hash = await shell(
                f'nix hash file --base32 --type sha256 {tmp_apfn}', True)

        project_apdn = get_project_apdn(project_hash, "upload")

        Lr.info(f'working directory: {project_apdn}')

        project_data_apfn = project_apdn / "project.json"
        project_data = DictFile(project_data_apfn)

        if project_apdn.exists():
            if not project_data:
                tmp_apfn.unlink()
                raise RuntimeError(f'{project_data_apfn} is missing')
        else:
            project_apdn.mkdir(parents=True, exist_ok=False)

        project_data["archive_rpfn"] = "archive.nar"
        nar_apfn = project_apdn / project_data["archive_rpfn"]
        tmp_apfn.rename(nar_apfn)

    archive_size = nar_apfn.stat().st_size

    Lr.info(f'archive size: {sizeof_fmt_si(archive_size)} ')

    # project_hash = await shell(
    #         f'nix hash file --base32 --type sha1 {nar_apfn}', True)
    project_hash_b64 = await sha256_b64(nar_apfn)
    await shell(
            f'nix hash file --base64 --type sha256 {nar_apfn}', True)

    project_data["project_root"] = str(project_apdn)
    project_data["project_hash"] = project_hash

    vault = S3Vault(project_data)
    await vault.refresh_index()
    vault_cfg = g.config["vaults"][g.config["active_vault"]]
    if (
        (old_metadata := vault.index.get(project_data["project_hash"])) and 
        old_metadata["archive_size"] == archive_size
    ):
        do_upload = False
    else:
        do_upload = True
        old_metadata = {}

    nix_version = await shell("nix --version", True)

    metadata = deepcopy(old_metadata) 

    if g.args.content_type:
        metadata["content_type"] = g.args.content_type
    elif not metadata.get("content_type"):
        metadata["content_type"] = await shell(
                f'file --brief --mime \'{g.args.path}\'', True)
    if g.args.comment:
        metadata["comment"] = g.args.comment
    if g.args.tags:
        metadata["tags"] = g.args.tags
    if g.args.name:
        metadata["name"] = g.args.name

    if "upload_timestamp" not in metadata or g.args.force:
        metadata["upload_timestamp"] = time()

    metadata["metadata_mtime"] = time()
    metadata["source_mtime"] = Path(g.args.path).stat().st_mtime
    metadata["source_path"] = str(Path(g.args.path).absolute())

    metadata["archiver_sw"] = "nix"
    metadata["archiver_sw_version"] = nix_version
    metadata["archive_format"] = "nar"
    metadata["archive_name"] = "archive.nar"
    metadata["archive_size"] = archive_size
    metadata["archive_hash"] = project_hash_b64

    if do_upload or g.args.force:

        if g.args.no_compression:
            Lr.info("skipping compression step ...")
            project_data["compressed_rpfn"] = project_data["archive_rpfn"]
        else:
            compressor = XZCompressor(project_data)
            await compressor.compress()
            metadata["compressor_sw"] = await compressor.get_name()
            metadata["compressor_sw_version"] = await compressor.get_version()
            metadata["compression_format"] = await compressor.get_format()
            compressed_apfn = project_apdn / project_data["compressed_rpfn"]
            size = compressed_apfn.stat().st_size
            Lr.info(f'compressed archive size: {sizeof_fmt_si(size)}')

        encrypter = SymmetricGPGEncrypter(project_data)
        await encrypter.encrypt()
        metadata["encrypter_sw_version"] = await encrypter.get_version()
        metadata["encrypter_sw"] = await encrypter.get_name()
        vault_cfg = g.config["vaults"][g.config["active_vault"]]
        if (encryption_key := vault_cfg.get("encryption_key")):
            metadata["encryption_key_name"] = vault_cfg["encryption_key"]
            metadata["encryption_key_public"] = \
                    g.encryption_keys[vault_cfg["encryption_key"]]["public"]
        metadata["encrypted_hash"] = project_data["encrypted_hash"]
        metadata["object_name"] = project_data["encrypted_rpfn"]
        encrypted_apfn = project_apdn / project_data["encrypted_rpfn"] 
        metadata["object_size"] = encrypted_apfn.stat().st_size
        metadata["storage_class"] = vault.standardize_storage_class(
                g.args.storage_class or vault_cfg["default_storage_class"])

        if (
            "storage_class" in old_metadata and
            metadata["storage_class"] != old_metadata["storage_class"]
        ):
            Lr.info(f'changing storage class: new={metadata["storage_class"]}, '
                    f'old={old_metadata["storage_class"]}')
            res = await prompt_bool("continue? ")
            if not res:
                return

        Lr.info("uploading data to vault ...")
        await vault.upload()
        Lr.info("upload finished")
    else:
        Lr.info("data is already uploaded")

    Lr.info("updating vault's metadata ...")
    metadata = {k: v for k, v in metadata.items() if v is not None}
    Lr.info(pformat(metadata))
    Lr.info(f'object id: {project_data["project_hash"]}')

    if g.args.name and g.args.replace:
        for k, v in list(vault.index.items()):
            if k == project_hash:
                continue
            if v.get("name") == g.args.name:
                Lr.info(f'old object with same name found:\n{pformat(v)}')
                res = await prompt_bool("delete? ")
                if not res:
                    continue
                del vault.index[k]

    vault.index[project_hash] = metadata

    await vault.save_index()
    await vault.close_connection()

    if not g.args.keep_tmp:
        Lr.info(f'deleting temporary directory {project_apdn} ...')
        shutil.rmtree(str(project_apdn))

    Lr.info("done")

    # if size >= 16 * 1024 ** 2:
    #     Lr.info(f'splitting to {g.args.part_size} parts ...')
    #     await shell(f'split --bytes={g.args.part_size} --numeric-suffixes=1 '
    #           f'{compressed_apfn} {compressed_apfn}.')
    # else:
    #     pass


async def action_restore():

    project_hash, metadata = await search_and_select_object()
    if not project_hash:
        return

    project_apdn = get_project_apdn(project_hash, "download")
    Lr.info(f'working directory: {project_apdn}')
    project_data_apfn = project_apdn / "project.json"
    project_data = DictFile(project_data_apfn)
    if project_apdn.exists():
        if not project_data:
            raise RuntimeError(f'{project_data_apfn} is missing')
    else:
        project_apdn.mkdir(parents=True, exist_ok=False)

    project_data["project_root"] = str(project_apdn)
    project_data["project_hash"] = project_hash
    project_data["metadata"] = metadata
    
    Lr.info("downloading object ...")
    vault = S3Vault(project_data)
    await vault.download()
    await vault.close_connection()
    
    if metadata["encrypter_sw"] == "age":
        encrypter = SymmetricGPGEncrypter(project_data)
        await encrypter.decrypt()
    else:
        raise ValueError(
                f'unrecognized encryption software: {metadata["encrypter_sw"]}')

    if "compressor_sw" not in metadata:  # no compression
        pass
    elif metadata["compressor_sw"] == "xz":
        compressor = XZCompressor(project_data)
        await compressor.decompress()
    else:
        raise ValueError(
                f'unrecognized compression software: {metadata["compressor_sw"]}')

    archive_apfn = project_apdn / metadata["archive_name"]
    archive_hash = await sha256_b64(archive_apfn)
    assert archive_hash == metadata["archive_hash"]

    if not g.args.dest:
        destination_apxn = Path(metadata["source_path"])
    else:
        destination_apxn = Path(g.args.dest).absolute()

    if destination_apxn.is_dir():
        Lr.error(f'{destination_apxn} directory already exists')
        return
    if destination_apxn.exists():
        res = await prompt_bool(f'{destination_apxn} already exists, overwrite? ')
        if not res:
            return
        destination_apxn.unlink()
    
    await shell(f'cat {archive_apfn} | nix-store --restore {destination_apxn}')
    Lr.info(f'object restored to {destination_apxn}')

    if not g.args.keep_tmp:
        Lr.info(f'deleting temporary directory {project_apdn} ...')
        shutil.rmtree(str(project_apdn))

    Lr.info("done")


async def action_search():
    raw_metadata = await search_for_matching_objects()
    if g.args.json:
        print(json.dumps(raw_metadata))
        return
    if not raw_metadata:
        Lr.info("no matching search results")
        return
    print_search_results(raw_metadata, False)    


async def action_vault():
    if g.args.vault not in g.config["vaults"]:
        Lr.error(f'vault "{g.args.vault}" does not exist')
        return
    g.config["active_vault"] = g.args.vault
    save_config()


async def action_stats():
    vault = S3Vault(None)
    await vault.print_stats()
    await vault.close_connection()


async def action_defreeze():

    project_hash, metadata = await search_and_select_object()
    if not project_pash:
        return

    project_data = {}
    project_data["project_hash"] = project_hash
    project_data["metadata"] = metadata
    vault = S3Vault(project_data)
    await vault.defreeze()
    await vault.close_connection()


async def action_delete():

    if g.args.multi:
        raw_metadata = await search_for_matching_objects()
    else:
        project_hash, metadata = await search_and_select_object()
        if project_hash:
            raw_metadata = { project_hash: metadata }
        else:
            return

    if not raw_metadata:
        Lr.info("no matching search results")
        return

    vault = S3Vault(None)
    await vault.refresh_index()
    for project_hash, metadata in raw_metadata.items():
        Lr.info(pformat(metadata))
        res = await prompt_bool("delete? ")
        if not res:
            continue
        del vault.index[project_hash]
    await vault.save_index()
    await vault.close_connection()


async def action_truncate():

    vault = S3Vault(None)
    await vault.refresh_index()
    if not vault.index:
        return

    if "all" in g.args.targets:
        g.args.targets = [
            "index",
            "index_versions",
            "objects",
            "object_versions",
            "unfinished_uploads",
        ]

    if "index" in g.args.targets:
        await vault.truncate_index()
    if "index_versions" in g.args.targets:
        await vault.truncate_index_versions()
    if "objects" in g.args.targets:
        await vault.truncate_objects()
    if "object_versions" in g.args.targets:
        await vault.truncate_object_versions()
    if "unfinished_uploads"in g.args.targets:
        await vault.delete_unfinished_uploads()
    await vault.close_connection()


###############################################################################


def load_config():
    config_apfn = xdg.xdg_config_home() / "cryo" / "config.toml"
    with config_apfn.open() as f:
        g.config = tomlkit.load(f)
        

def save_config():
    config_apfn = xdg.xdg_config_home() / "cryo" / "config.toml"
    with config_apfn.open("w") as f:
        tomlkit.dump(g.config, f)


def load_keys():
    g.encryption_keys = {}
    for key_name, key_data in g.config["keys"].items():
        if key_data["key_type"] == "age":
            d = {}
            d["path"] = Path(key_data["path"])
            with d["path"].open() as f:
                data = f.read()
            d["public"] = re.search(r"public key: ([a-z0-9]+)", data).group(1)
            g.encryption_keys[key_name] = d
        else:
            raise ValueError(f'unknown key type: {key_data["key_type"]}')


def _setup_logging() -> None:

    log_settings = {"level": g.args.log_level}
    disabled_modules = [
        "parso",
        "websockets",
        "asyncio",
        "matplotlib",
        "blib2to3"
    ]
    name = "cryo"

    class MyFormat(LoggingFormat):
        @classmethod
        def FORMAT(cls):
            def gen_log_msg(record):
                s = ""
                if g.args.log_timestamps:
                    s += f'{cls.TIME} '
                s += f'{cls.EXTRA(record)}'
                s += f'{cls.MESSAGE}'
                s += f'{cls.END(record)}'
                return s
            return gen_log_msg
            # return lambda record: (
            #     f'{cls.TIME} ' if g.args.log_timestamps else ''
            #     # f'{cls.LEVEL} '
            #     # f'{cls.NAME} '
            #     f'{cls.EXTRA(record)}'
            #     f'{cls.MESSAGE}'
            #     f'{cls.END(record)}'
            # )
            # return lambda record: (
            #     f'{cls.TIME} ' if g.args.log_timestamps else ''
            #     # f'{cls.LEVEL} '
            #     # f'{cls.NAME} '
            #     f'{cls.EXTRA(record)}'
            #     f'{cls.MESSAGE}'
            #     f'{cls.END(record)}'
            # )

    setup_logging(
        Lr,
        log_level=g.args.log_level,
        disabled_modules=disabled_modules,
        format=MyFormat()
    )


def parse_args():

    desc = "cryo -- a backup tool"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument(
        "--concurrency",
        type=int,
        default=5,
        help="concurrency setting for transfers"
    )
    parser.add_argument(
        "--chunk-size",
        type=int,
        default=100*1024**2,
        help="chunk size for file transfers"
    )
    parser.add_argument(
        "--confirm-all",
        action="store_true",
        help="auto confirm yes/no prompts"
    )
    parser.add_argument(
        "--log-timestamps",
        action="store_true",
        help="show timestamps on logs"
    )
    parser.add_argument(
        "--show-elapsed",
        action="store_true",
        help="print elapsed time at the end"
    )
    parser.add_argument("--debug", action="store_true", help="debug mode")
    parser.add_argument(
        "--log-level", default="INFO", help="logging verbosity",
    )

    def add_search_args(parser):
        parser.add_argument(
            "generic_kw",
            nargs="?",
            help="generic kw to search"
        )
        parser.add_argument(
            "--tags",
            help="tags to search"
        )
        parser.add_argument(
            "--sort-by",
            help="sort by column"
        )

    subparsers = parser.add_subparsers(dest="action", required=True)

    parser_freeze = subparsers.add_parser("freeze", help="backup to vault")
    parser_freeze.add_argument("path", nargs="?", help="path to freeze")
    parser_freeze.add_argument(
        "--project",
        help="manual project override for continuation"
    )
    parser_freeze.add_argument(
        "--delete",
        action="store_true",
        help="delete path after done"
    )
    parser_freeze.add_argument(
        "--name",
        help="object name"
    )
    parser_freeze.add_argument(
        "--compression-args",
        help="compression args to use"
    )
    parser_freeze.add_argument(
        "--storage-class",
        help="storage class for upload"
    )
    parser_freeze.add_argument(
        "--comment",
        help="comment for metadata"
    )
    parser_freeze.add_argument(
        "--tags",
        help="tags for metadata"
    )
    parser_freeze.add_argument(
        "--force",
        action="store_true",
        help="force upload"
    )
    parser_freeze.add_argument(
        "--replace",
        action="store_true",
        help="replace old objects with the same name"
    )
    parser_freeze.add_argument(
        "--no-compression",
        action="store_true",
        help="skip compression step"
    )
    parser_freeze.add_argument(
        "--keep-tmp",
        action="store_true",
        help="do not remove temporary files"
    )
    parser_freeze.add_argument(
        "--content-type",
        help="mime content type for metadata"
    )
    parser_freeze.add_argument(
        "--min-part-size",
        type=int,
        default=5*1024**2,
        help="minimum part size in bytes",
    )
    parser_freeze.set_defaults(fun=action_freeze)

    parser_defreeze = subparsers.add_parser("defreeze", help="defreeze an object")
    add_search_args(parser_defreeze)
    parser_defreeze.add_argument(
        "--speed",
        default="Standard",
        help="speed of defreezing"
    )
    parser_defreeze.add_argument(
        "--days",
        type=int,
        default=7,
        help="days to keep"
    )
    parser_defreeze.add_argument(
        "--wait",
        action="store_true",
        help="wait for completion"
    )
    parser_defreeze.set_defaults(fun=action_defreeze)

    parser_delete = subparsers.add_parser("delete", help="delete object(s)")
    add_search_args(parser_delete)
    parser_delete.add_argument(
        "--multi",
        action="store_true",
        help="delete all matching objects"
    )
    parser_delete.set_defaults(fun=action_delete)

    parser_restore = subparsers.add_parser("restore", help="restore from vault")
    add_search_args(parser_restore)
    parser_restore.add_argument(
        "dest",
        nargs="?",
        help="destination path"
    )
    parser_restore.add_argument(
        "--keep-tmp",
        action="store_true",
        help="do not remove temporary files"
    )
    parser_restore.set_defaults(fun=action_restore)

    parser_search = subparsers.add_parser("search", help="search vault contents")
    add_search_args(parser_search)
    parser_search.add_argument(
        "--json",
        action="store_true",
        help="json output"
    )
    parser_search.set_defaults(fun=action_search)

    parser_truncate = subparsers.add_parser("truncate", help="truncate data")
    parser_truncate.add_argument(
        "targets",
        nargs="+",
        help="truncation targets"
    )
    parser_truncate.set_defaults(fun=action_truncate)

    parser_vault = subparsers.add_parser("vault", help="select vault")
    parser_vault.add_argument("vault", help="vault to select")
    parser_vault.set_defaults(fun=action_vault)

    parser_stats = subparsers.add_parser("stats", help="print stats")
    parser_stats.set_defaults(fun=action_stats)

    args = parser.parse_args()

    if args.__dict__.get("tags"):
        assert re.fullmatch(r"[a-z_ ]+", args.tags)
        args.tags = args.tags.split()

    return args


def adjust_min_part_size():
    if g.args.__dict__.get("min_part_size"):
        g.args.min_part_size = g.config.get(
                "min_part_size", g.args.min_part_size)
        if g.args.min_part_size < 5 * 1024 ** 2:
            g.args.min_part_size = 5 * 1024 ** 2
            Lr.warning("min part size too small, reverting to default")
        Lr.info(f'using min part size: {g.args.min_part_size:,}')


async def async_main():
    ts_start = monotonic()
    res = await wait_first(g.args.fun(), g.shutdown_event.wait())
    if g.args.show_elapsed:
        elapsed = Timedelta(seconds=monotonic() - ts_start)
        Lr.info(f'total time elapsed: {elapsed}')


def main():
    g.args = parse_args()
    _setup_logging()
    load_config()
    adjust_min_part_size()
    load_keys()
    exit_code = run_app(
            async_main(), g.args.debug, shutdown_event=g.shutdown_event)
    sys.exit(exit_code)


if __name__ == "__main__":
    main()
