import numpy as np
import inspect

from copy import deepcopy
from functools import reduce
from operator import getitem

from .asyncio import create_managed_task

import typing as t

A = t.TypeVar('A')
B = t.TypeVar('B')
C = t.TypeVar('C')
D = t.TypeVar('D')


def running_from_ipython():
    """Ref: https://stackoverflow.com/a/5377051/1793556"""
    try:
        __IPYTHON__
        return True
    except NameError:
        return False


def dict_drop_if_exists(dic, key, errors=False, inplace=False):
    """Drop given key(s) `key` from `dict` `dic`, if they exist."""
    
    if not inplace:
        dic = deepcopy(dic)
        
    keys = np.array([key]).flatten()
    for k in keys:
        if k in dic:
            dic.pop(k)
        elif errors:
            raise ValueError(f'Key `{k}` not in `dic`')
    
    return dic


def dict_drop_value(
    dic: dict[A, B],
    value: t.Union[B, t.Iterable[B]],
    inplace=False,
) -> dict[A, B]:
    """Drop elements with given value(s) `value` from `dic`.
    
    Examples:
    >>> dict_drop_value({}, 1)
    {}
    >>> dict_drop_value({'a': 1}, 1)
    {}
    >>> dict_drop_value({'a': 1}, 2)
    {'a': 1}
    >>> dict_drop_value({'a': 1, 'b': None}, None)
    {'a': 1}
    >>> dict_drop_value({'a': 1, 'b': 2, 'c': 3}, [1, 3])
    {'b': 2}
    """
    if not inplace:
        dic = deepcopy(dic)
    
    values = np.array([value]).flatten()
    to_remove = [k for k, v in dic.items() if v in values]
    for k in to_remove:
        dic.pop(k)
    
    return dic


def df_drop_if_exists(df, key, axis=0, errors=False, inplace=False):
    """Drop given key(s) from `DataFrame` `df`, if they exist."""

    if not inplace:
        df = deepcopy(df)

    keys = np.array([key]).flatten()
    for k in keys:
        if k in df:
            df.drop(k, axis=axis, inplace=True)
        elif errors:
            raise ValueError(f'Key `{k}` not in `df`')

    return df


def dict_apply_level0(
    dic: dict[A, B],
    fun: t.Callable[[B], C],
) -> dict[A, C]:
    """Apply function `fun` to values of `dic`, at level 0.
    
    Examples:
    >>> import numpy as np
    >>> import pandas as pd
    >>>
    >>> dict_apply_level0(
    ...     {'a': 1, 'b': 2, 'c': 3},
    ...     lambda x: 2 * x
    ... )
    {'a': 2, 'b': 4, 'c': 6}
    >>> dict_apply_level0(
    ...     {'a': 1, 'b': np.nan, 'c': 3},
    ...     lambda x: None if pd.isna(x) else x
    ... )
    {'a': 1, 'b': None, 'c': 3}
    """
    return {k: fun(v) for k, v in dic.items()}


def dict_apply_level1(
    dic: dict[A, dict[B, C]],
    fun: t.Callable[[C], D],
) -> dict[A, dict[B, D]]:
    """Apply function `fun` to values of `dic`, at level 1.
    
    Examples:
    >>> dict_apply_level1(
    ...     {'a': {'x': 1, 'y': 2}, 'b': {'z': 3, 'w': 4}},
    ...     lambda x: 2 * x
    ... )
    {'a': {'x': 2, 'y': 4}, 'b': {'z': 6, 'w': 8}}
    """
    return {
        k: dict_apply_level0(inner_dict, fun)
        for k, inner_dict in dic.items()
    }


def dict_apply(
    dic: dict,
    fun: t.Callable,
    level: int = 0,
) -> dict:
    """Apply function `fun` to values of `dic`, at given level.
    
    Examples:
    >>> dict_apply(
    ...     {'a': 1, 'b': 2, 'c': 3},
    ...     lambda x: 2 * x,
    ...     level=0,
    ... )
    {'a': 2, 'b': 4, 'c': 6}
    >>> dict_apply(
    ...     {'a': {'x': 1, 'y': 2}, 'b': {'z': 3, 'w': 4}},
    ...     lambda x: 2 * x,
    ...     level=1,
    ... )
    {'a': {'x': 2, 'y': 4}, 'b': {'z': 6, 'w': 8}}
    """
    if level == 0:
        apply = dict_apply_level0
    elif level == 1:
        apply = dict_apply_level1
    else:
        raise ValueError(f'Invalid value `{level=}`')
        
    return apply(dic, fun)
    

def nested_get(
    dic: dict[A, B],
    keys: list[A],
) -> B:    
    
    return reduce(getitem, keys, dic)


def execute_fn_or_afn(fn_or_afn, **kwargs):
    if inspect.iscoroutinefunction(fn_or_afn):
        create_managed_task(fn_or_afn(), **kwargs)
    else:
        fn_or_afn()


def first(iterable: t.Iterable[A]) -> t.Optional[A]:
    """Return the first element from an (ordered) iterable. 
    
    :param iterable: iterable to go through
    :return: first element if `iterable` is ordered, random element otherwise
    
    Examples:
    >>> first(('b', 'a', 'c'))
    'b'
    >>> first(['b', 'a', 'c'])
    'b'
    >>> first({'b': 1, 'a': 2, 'c': 3})
    'b'
    >>> from sortedcontainers import SortedDict
    >>> sd = SortedDict({'b': 1, 'a': 2, 'c': 3})
    >>> first(sd)
    'a'
    >>> first([]) is None
    True
    """
    try:
        return next(iter(iterable))
    except StopIteration:
        return None
    