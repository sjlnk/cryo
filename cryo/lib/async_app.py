import asyncio
import sys
import signal

from loguru import logger as L
from traceback import (
    format_exception,
)
from .misc import (
    running_from_ipython,
)
from .asyncio import (
    create_managed_task,
)

import typing as t


def run_app(
    main_coro: t.Coroutine,
    debug_mode: bool = False,
    slow_callback_duration: float = 0.1,
    shutdown_event: t.Optional[asyncio.Event] = None,
    cleanup_coro: t.Optional[t.Coroutine] = None,
) -> None:

    if not shutdown_event:
        shutdown_event = asyncio.Event()

    loop = asyncio.get_event_loop()

    interrupted_with_signal = None

    def shutdown_signal_handler(signal):
        nonlocal interrupted_with_signal
        interrupted_with_signal = signal
        L.info(f'interrupted with signal: {signal.name}')
        shutdown_event.set()
        if cleanup_coro:
            create_managed_task(cleanup_coro)

    loop.add_signal_handler(
        signal.SIGINT, lambda: shutdown_signal_handler(signal.SIGINT),
    )
    loop.add_signal_handler(
        signal.SIGTERM, lambda: shutdown_signal_handler(signal.SIGTERM),
    )

    if debug_mode:
        L.info("setting event loop to debugging mode ...")
        loop.slow_callback_duration = slow_callback_duration
        loop.set_debug(True)

    def evloop_exception_handler(loop, context):
        if "underlying_task" in context:
            msg = f'exception on a managed task {context["underlying_task"]}:\n'
            err = context["exception"]
            msg += "\n".join(format_exception(type(err), err, err.__traceback__))
            if context.get("critical"):
                L.critical(msg)
                shutdown_event.set()
                if cleanup_coro:
                    asyncio.create_task(cleanup_coro)
                else:
                    sys.exit(1)
            else:
                L.error(msg)
        else:
            loop.default_exception_handler(context)

    loop.set_exception_handler(evloop_exception_handler)

    if running_from_ipython():
        # Facilitates post mortem debugging if main_coro
        # throws when ran via ipython.
        res = loop.run_until_complete(main_coro)
    else:
        try:
            res = loop.run_until_complete(main_coro)
        except:
            L.critical("exception on main coroutine:")
            if cleanup_coro:
                loop.run_until_complete(cleanup_coro)
            raise

    if interrupted_with_signal is not None:
        return interrupted_with_signal.value + 128

    if not res:
        res = 0
    if not isinstance(res, int):
        raise ValueError(
                f'unexpected return value type from main coro: {type(res)}')
    return res
