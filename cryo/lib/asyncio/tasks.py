import asyncio

import typing as t


def create_managed_task(coro_obj: t.Coroutine, **kwargs) -> asyncio.Task:
    critical = kwargs.pop("critical", False)
    async def managed_task():
        try:
            return await coro_obj
        except Exception as err:
            asyncio.get_event_loop().call_exception_handler({
                "message": str(err),
                "exception": err,
                "critical": critical,
                "underlying_task": task,
            })
            return err
    task = asyncio.create_task(managed_task(), **kwargs)
    return task
