import pandas as pd
import asyncio

from loguru import logger as L
from traceback import format_exception


def setup_asyncio_notebook(shutdown_event=None, cleanup_afn=None):

    """Enables support for managed tasks.

    Parameters:
    -----------
    shutdown_event : asyncio.Event (optional)
        Will be set if a critical managed task throws an unhandled exception.
    cleanup_afn : coroutine (optional)
        Will be run as a vanilla background task if a critical managed task
        throws an unhandled exception.
    """
    
    def evloop_exception_handler(loop, context):
        if "underlying_task" in context:
            msg = f'exception on a managed task {context["underlying_task"]}:\n'
            err = context["exception"]
            msg += "\n".join(format_exception(type(err), err, err.__traceback__))
            if context.get("critical"):
                L.critical(msg)
                if shutdown_event:
                    shutdown_event.set()
                if cleanup_afn:
                    asyncio.create_task(cleanup_afn())
            else:
                L.error(msg)
        else:
            loop.default_exception_handler(context)
        
    asyncio.get_event_loop().set_exception_handler(evloop_exception_handler)


async def sleep(td: pd.Timedelta) -> None:
    """Sleep for time `td`."""
    await asyncio.sleep(td / pd.Timedelta(1, 'second'))
