import asyncio


# used to signal that task was actually canceled
CANCELLED_OBJECT = object()
async def swallow_cancel(aw):
    try:
        return await aw
    except asyncio.CancelledError:
        return CANCELLED_OBJECT


async def wait_first(*awaitables):

    """Wait for first awaitable to complete and return it's result."""
 
    # Shield the awaitables from cancellation and ignore cancellation errors.
    # Shielded awaitables are wrapped inside tasks for compatibility 
    # with asyncio.wait.
    awaitables = [asyncio.create_task(swallow_cancel(x)) for x in awaitables]
    done, pending = await asyncio.wait(
            awaitables, return_when=asyncio.FIRST_COMPLETED)
    
    # Rest of the awaitables are cancelled and awaited to prevent memory leaks.
    for task in pending:
        task.cancel()
        await task
        
    return await next(iter(done))


# https://stackoverflow.com/a/55531688/1793556
async def as_completed(futures):

    loop = asyncio.get_event_loop()
    wrappers = []
    for fut in futures:
        assert isinstance(fut, asyncio.Future)  # we need Future or Task
        # Wrap the future in one that completes when the original does,
        # and whose result is the original future object.
        wrapper = loop.create_future()
        fut.add_done_callback(wrapper.set_result)
        wrappers.append(wrapper)

    for next_completed in asyncio.as_completed(wrappers):
        # awaiting next_completed will dereference the wrapper and get
        # the original future (which we know has completed), so we can
        # just yield that
        yield await next_completed
