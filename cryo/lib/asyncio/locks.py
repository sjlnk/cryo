import asyncio.events
import collections
import threading
from contextlib import asynccontextmanager


###############################################################################


_loop_bound_mixin_global_lock = threading.Lock()


class _LoopBoundMixin:


    _loop = None


    def _get_loop(self):
        loop = asyncio.events._get_running_loop()
        if self._loop is None:
            with _loop_bound_mixin_global_lock:
                if self._loop is None:
                    self._loop = loop
        if loop is not self._loop:
            raise RuntimeError(f'{self!r} is bound to a different event loop')
        return loop


###############################################################################


class TwoTierLock(_LoopBoundMixin):


    def __init__(self):
        super().__init__()
        # privileged
        self._p_waiters = collections.deque()
        self._p_running = set()
        # unprivileged
        self._up_waiters = collections.deque()
        self._up_running = set()


    def __repr__(self):
        s = f'<{self.__class__.__name__}@{id(self)} '
        s += f'[p={len(self._p_waiters)}/{len(self._p_running)}, '
        s += f'up={len(self._up_waiters)}/{len(self._up_running)}'
        s += "]>"
        return s


    def __call__(self, privileged_access):
        @asynccontextmanager
        async def ctxmanager():
            await self.acquire(privileged_access)
            try:
                yield
            finally:
                self.release()
        return ctxmanager()


    def waiter_count(self):
        return len(self._p_waiters) + len(self._up_waiters)


    async def acquire(self, privileged_access):
        
        loop = self._get_loop()
        task = asyncio.current_task()

        # print(f'{self} acquire {task.get_name()=}, {privileged_access=}')

        if privileged_access:

            while self._up_running:
                fut = loop.create_future()
                self._p_waiters.append(fut)
                await fut

            assert task not in self._p_running
            self._p_running.add(task)

        else:

            while self._p_running or self._up_running:
                fut = loop.create_future()
                self._up_waiters.append(fut)
                await fut

            assert task not in self._up_running
            self._up_running.add(task)


    def release(self):

        task = asyncio.current_task()

        # print(f'{self} release {task.get_name()=}')

        self._p_running.discard(task)
        self._up_running.discard(task)

        assert not self._up_running

        if self._p_waiters:
            fut = self._p_waiters.popleft()
            fut.set_result(True)

        elif self._up_waiters and not self._p_running and not self._up_running:
            fut = self._up_waiters.popleft()
            fut.set_result(True)

