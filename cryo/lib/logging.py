import loguru
import sys
import logging
from loguru import logger


class InterceptHandler(logging.Handler):


    """Intercept standard `logging` messages and guide them through `Loguru`.

    Ref: https://loguru.readthedocs.io/en/stable/overview.html?highlight=intercept#entirely-compatible-with-standard-logging
    """


    def emit(self, record: logging.LogRecord) -> None:
        # Get corresponding Loguru level if it exists
        try:
            level = logger.level(record.levelname).name
        except ValueError:
            level = record.levelno

        # Find caller from where originated the logged message
        frame, depth = logging.currentframe(), 2
        while frame.f_code.co_filename == logging.__file__:
            frame = frame.f_back
            depth += 1

        logger.opt(depth=depth, exception=record.exc_info).log(
            level, record.getMessage(),
        )



class Format:

    TIME    = '[<green>{time:YYYY-MM-DD HH:mm:ss.SSS!UTC}</>]'
    LEVEL   = '[<level>{level: <9}</>]'
    NAME    = '[<cyan>{name}</>]'
    MESSAGE = '<level>{message}</>'

    @classmethod
    def EXTRA(cls, record) -> str:
        extra = ''
        for extra_value in record['extra'].values():
            if extra_value is not None:
                extra += f'[<cyan>{extra_value}</>] '
        return extra
    
    @classmethod
    def END(cls, record) -> str:
        if record['exception'] is not None:
            return '\n{exception}'
        return '\n'

    @classmethod
    def FORMAT(cls):
        return lambda record: (
            f'{cls.TIME} '
            f'{cls.LEVEL} '
            f'{cls.NAME} '
            f'{cls.EXTRA(record)}'
            f'{cls.MESSAGE}'
            f'{cls.END(record)}'
        )



def setup_logging(
    root_logger,
    log_level,
    format: Format = Format(),
    disabled_modules: list[str] = None,
):

    # guide standard `logging` messages through `Loguru`
    logging.basicConfig(handlers=[InterceptHandler()], level=log_level)

    root_logger.remove()
    for module in disabled_modules:
        root_logger.disable(module)
    
    fmt = format.FORMAT()

    # console
    console_handler_id = root_logger.add(
        sys.stderr,
        format=fmt,
    )
