from setuptools import setup, find_packages

entry_points={
    "console_scripts": [ 
        "cryo = cryo.main:main",
    ]
}

setup(name="cryo",
      version="0.0.1",
      packages=find_packages("."),
      entry_points=entry_points,
)

